我们根据第一个项目spring-cloud-trip-config作为基础，增加eureka作为服务注册中心，分为3个module

* eureka-server，作为eureka的server服务注册中心
* eureka-config-server，注册为config-server和eureka-client端，连接到eureka-server成为服务提供者。
* eureka-config-client，作为config-client和eureka-client端，连接到eureka-server成为服务消费者。

> eureka服务端搭建（module：eureka-server）

1、pom.xml依赖

``` java
<dependency>
    <groupId>org.springframework.cloud</groupId>
    <artifactId>spring-cloud-starter-netflix-eureka-server</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

2、application入口类添加注解@EnableEurekaServer

3、application.yml配置

``` xml
spring:
  security:
    user:
      name: user                # 配置登录的账号是user
      password: pwd     # 配置登录的密码是pwd
server:
  port: 8761                    # 指定该Eureka实例的端口
eureka:
  client:             #用于连接发送心跳包，以及集群互联
    registerWithEureka: false
    fetchRegistry: false
    serviceUrl:
      defaultZone: http://user:pwd@localhost:8761/eureka/
```

4、如果使用的spring cloud是Finchley版，且需要鉴权的话，需要增加类，部分关闭掉Spring Security的CSRF保护功能。可查看官方文档说明。

``` java
@EnableWebSecurity
class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().ignoringAntMatchers("/eureka/**");
        super.configure(http);
    }
}
```

5、 运行并测试

使用浏览器访问http://localhost:8761 可查看eureka页面。

6、 集群

可做集群来保证高可用性，相互注册即可。

> eureka客户端搭建（两个module：eureka-config-server，eureka-config-client）

1、微服务如果需要注册到eureka上，需要启用注解@EnableEurekaClient，并在yml上配置如下

```bash
eureka:
  client:
    serviceUrl:
      defaultZone: http://user:pwd@localhost:8761/eureka/
```

成功注册后可在页面上查看注册方信息。分别是两个客户端module的注册内容，名称通过yml文件的spring.application.name设置。